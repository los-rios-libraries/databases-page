showNote({
	cid: 'college_outage',
	message: 'Access to databases may be interrupted on Thursday, August 11 from 9:00 pm to midnight.',
	//start: '2018-05-18',
	end: '2022-08-11'  
});
showNote({
	cid: 'do_outage',
	message: 'Access to databases may be interrupted on Sunday, August 14 from 6:00 am to 9:00 am.',
	start: '2022-08-13',
	end: '2022-08-15'  
});